import axios from '../../../../plugins/http'

const state = {
  logConversa: [],
  perguntaChat: 0
}

const getters = {
  logConversa: state => state.logConversa,
  perguntaChat: state => state.perguntaChat
}

const types = {
  ZERAR_LOGCONVERSA: 'ZERAR_LOGCONVERSA',
  ADICIONAR_LOGCONVERSA: 'ADICIONAR_LOGCONVERSA',
  MUDAR_PERGUNTACHAT: 'MUDAR_PERGUNTACHAT'
}

const mutations = {
  [types.ADICIONAR_LOGCONVERSA] (state, { dialogo }) {
    state.logConversa.push(dialogo)
  },
  [types.ZERAR_LOGCONVERSA] (state) {
    state.logConversa = []
  },
  [types.MUDAR_PERGUNTACHAT] (state, perguntaChat) {
    state.perguntaChat = perguntaChat
  }
}

function tirarDecimais (val, simb = '') {
  if (val !== '') {
    if (simb === '') {
      return val.toFixed(2)
    } else {
      return val.toFixed(0) + '' + simb
    }
  } else {
    return ''
  }
}

function milhar (valor) {
  let k = '' + valor
  let n = '' + valor
  n = n.slice(0, k.length - 3)
  const t = n.length
  if (t <= 3) {
    return valor
  } else {
    for (let i = 1; i * 3 < t; i++) {
      n = n.slice(0, t - i * 3) + '.' + n.slice(t - i * 3, t)
    }
    return n + (k).slice(k.length - 3, k.length)
  }
}

function virgula (valor) {
  let n = '' + valor
  return n.replace('.', ',')
}

function conversa1Formatada (conversa, tarefa) {
  var texto = 'Você deve fazer: <br>'
  texto += '<ul>'
  for (var i = 0; i < conversa.nome.length; i++) {
    texto += '<li><strong>' + conversa.nome[i] + '</strong> '
    if (conversa.descricao.length !== 0 && conversa.descricao[i] !== '' && conversa.descricao[i] !== undefined) {
      texto += ': "' + conversa.descricao[i]
    }
    texto += '</li>'
  }
  texto += '</ul>'
  texto += '. Com isso você realiza a tarefa de <strong>' + tarefa + '</strong>'
  return texto
}

function conversa0Formatada (conversa, dica) {
  var texto = ''
  if (conversa.length !== 0) {
    texto += 'Veja o que busquei para você: <br>'
    texto += '<strong>' + conversa[0].titulo + '</strong><br>'
    if (conversa[0].tipo === 'LINK') {
      texto += '<a src="' + conversa[0].link + '" target="_blank">' + conversa[0].link + '</a>'
    } else if (conversa[0].tipo === 'IMAGEM') {
      texto += 'Veja a imagem que trouxe para você. <a src="' + conversa[0].urlImagem + '" target="_blank">Clique aqui.</a>'
    } else if (conversa[0].tipo === 'VIDEO') {
      texto += '<a src="' + conversa[0].link + '" target="_blank">' + conversa[0].link + '</a>'
    } else {
      texto += conversa[0].texto
    }
    texto += '<br>Fonte da informação: ' + conversa[0].fonte + '<br>'
    texto += '. Esta informação está relacionada a busca sobre <strong>' + dica + '</strong>.'
  } else {
    texto = 'Infelizmente não pude encontrar uma informação relacionada a sua busca  sobre ' + dica
  }
  return texto
}

function conversa2Formatada (conversa, perfilObra) {
  var texto = 'Os custos indiretos totais que você possui na obra <strong>' + perfilObra.nome + '</strong> são: <br>'
  texto += '<ul>'
  for (var prop in conversa) {
    console.log(prop)
    if (prop !== 'idPerfilObra') {
      texto += '<li> Em ' + prop + ' = ' + milhar(virgula(tirarDecimais(conversa[prop]))) + '</li>'
    }
  }
  texto += '</ul>'
  return texto
}

function conversa3Formatada (conversa, perfilObra) {
  var texto = 'Os custos indiretos  mensais que você possui na obra ' + perfilObra.nome + ' são: '
  texto += '<ul>'
  for (var prop in conversa) {
    console.log(prop)
    if (prop !== 'idPerfilObra') {
      texto += '<li> Em ' + prop + ' = ' + milhar(virgula(tirarDecimais(conversa[prop]))) + '</li>'
    }
  }
  texto += '</ul>'
  return texto
}

const actions = {
  zerarConversa ({commit}) {
    commit(types.ZERAR_LOGCONVERSA)
  },
  mudarPergunta ({commit}, { pergunta }) {
    commit(types.MUDAR_PERGUNTACHAT, pergunta)
  },
  conversa0 ({commit}, { dica }) {
    return new Promise(function (resolve, reject) {
      axios.get('/dicas/busca/' + dica)
      .then(function (response) {
        console.log(response)
        commit(types.ADICIONAR_LOGCONVERSA, {
          dialogo: {gero: conversa0Formatada(response.data, dica), cliente: 'O que você pode me dizer sobre <strong>' + dica + '</strong>?'}
        })
        resolve()
      })
      .catch(function (error) {
        console.log(error)
        reject()
      })
    })
  },
  conversa1 ({commit}, { tarefa }) {
    return new Promise(function (resolve, reject) {
      axios.get('/atividades/tarefa/' + tarefa)
      .then(function (response) {
        console.log(response)
        commit(types.ADICIONAR_LOGCONVERSA, {
          dialogo: {gero: conversa1Formatada(response.data, tarefa), cliente: 'O que devo fazer na tarefa <strong>' + tarefa + '</strong>?'}
        })
        resolve()
      })
      .catch(function (error) {
        console.log(error)
        reject()
      })
    })
  },
  conversa2 ({commit}, { perfilObra }) {
    return new Promise(function (resolve, reject) {
      axios.get('/custo/indireto/' + perfilObra.id)
      .then(function (response) {
        console.log(response)
        commit(types.ADICIONAR_LOGCONVERSA, {
          dialogo: {gero: conversa2Formatada(response.data, perfilObra), cliente: 'Quais os custos indiretos totais na obra <strong>' + perfilObra.nome + '</strong>?'}
        })
        resolve()
      })
      .catch(function (error) {
        console.log(error)
        reject()
      })
    })
  },
  conversa3 ({commit}, { perfilObra }) {
    return new Promise(function (resolve, reject) {
      axios.get('/custo/indireto/mensal/' + perfilObra.id)
      .then(function (response) {
        console.log(response)
        commit(types.ADICIONAR_LOGCONVERSA, {
          dialogo: {gero: conversa3Formatada(response.data, perfilObra), cliente: 'Quais os custo indiretos mensais na obra <strong>' + perfilObra.nome + '</strong>?'}
        })
        resolve()
      })
      .catch(function (error) {
        console.log(error)
        reject()
      })
    })
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
