import axios from '../../../../plugins/http'

const state = {
  perfilObras: [],
  tarefas: []
}

const getters = {
  perfilObras: state => state.perfilObras,
  tarefas: state => state.tarefas
}

const types = {
  TAREFAS: 'TAREFAS',
  MUDAR_PERFILOBRA: 'MUDAR_PERFILOBRA',
  MUDAR_PERFILOBRAS: 'MUDAR_PERFILOBRAS'
}

const mutations = {
  [types.TAREFAS] (state, { tarefas }) {
    state.tarefas = tarefas
  },
  [types.MUDAR_PERFILOBRA] (state, { perfilObra, index }) {
    state.perfilObras[index] = perfilObra
  },
  [types.MUDAR_PERFILOBRAS] (state, { perfilObras }) {
    state.perfilObras = perfilObras
  }
}

const actions = {
  mudarPerfilObra ({ commit }, { perfilObra, index }) {
    return new Promise(function (resolve, reject) {
      axios.put('/perfil-obras', { perfilObra })
      .then(function (response) {
        commit(types.MUDAR_PERFILOBRA, { perfilObra: perfilObra, index: index })
        resolve()
      })
      .catch(function (error) {
        console.log(error)
        reject(Error('It broke'))
      })
    })
  },
  pegarTarefas ({commit}) {
    return new Promise(function (resolve, reject) {
      axios.get('/test_vue')
      .then(function (response) {
        console.log(response)
        commit(types.TAREFAS, { tarefas: response.data })
        resolve()
      })
      .catch(function (error) {
        console.log(error)
        reject(Error('It broke'))
      })
    })
  },
  atualizarPerfilObras ({ commit }, idUsuario) {
    return new Promise(function (resolve, reject) {
      axios.get('/perfil-obras/usuario/' + idUsuario)
      .then(function (response) {
        commit(types.MUDAR_PERFILOBRAS, { perfilObras: response.data })
        resolve()
      })
      .catch(function (error) {
        console.log(error)
        reject(Error('It broke'))
      })
    })
  },
  excluirPerfilObra ({ commit }, { id, idUsuario }) {
    return new Promise(function (resolve, reject) {
      axios.delete('/perfil-obras/' + id)
      .then(function (response) {
        this.atualizarPerfilObras({ idUsuario: idUsuario })
        resolve()
      })
      .catch(function (error) {
        console.log(error)
        reject(Error('It broke'))
      })
    })
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
