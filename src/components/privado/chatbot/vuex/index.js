import axios from '../../../../plugins/http'
import lodash from 'lodash'

const state = {
  local: '',
  area: 0,
  pavimentos: -1,
  acabamento: -1,
  revestimento: -1,
  prazo: 0,
  custo: 0,
  efetivo: 0,
  nome: '',
  inicio: '',
  cronograma: [],
  passos: 0,
  carregando: false
}

const getters = {
  local: state => state.local,
  area: state => state.area,
  pavimentos: state => state.pavimentos,
  acabamento: state => state.acabamento,
  revestimento: state => state.revestimento,
  prazo: state => state.prazo,
  custo: state => state.custo,
  efetivo: state => state.efetivo,
  nome: state => state.nome,
  inicio: state => state.inicio,
  passos: state => state.passos,
  carregando: state => state.carregando,
  cronogramaRefatorado () {
    return state.cronograma !== [] ? lodash.map(state.cronograma, function (value) {
      value.meses = lodash.map(value.meses, function (n) {
        if (n === null) {
          return {'custo': '', 'porcentagemExecucao': ''}
        } else {
          return n
        }
      })
      return value
    }) : []
  },
  valorPlanejado () {
    if (state.cronograma.length === 0) {
      return []
    } else {
      let valores = []
      for (let j = 0; j < state.cronograma[0].meses.length; j++) {
        valores.push(0)
      }
      for (let j = 0; j < state.cronograma[0].meses.length; j++) {
        for (let i = 0; i < state.cronograma.length; i++) {
          valores[j] += state.cronograma[i]['meses'][j].custo === '' ? 0 : parseFloat(state.cronograma[i]['meses'][j].custo)
        }
      }
      return valores
    }
  }
}

const types = {
  MUDAR_LOCAL: 'MUDAR_LOCAL',
  MUDAR_AREA: 'MUDAR_AREA',
  MUDAR_PAVIMENTOS: 'MUDAR_PAVIMENTOS',
  MUDAR_ACABAMENTO: 'MUDAR_ACABAMENTO',
  MUDAR_REVESTIMENTO: 'MUDAR_REVESTIMENTO',
  MUDAR_INICIO: 'MUDAR_INICIO',
  MUDAR_NOME: 'MUDAR_NOME',
  MUDAR_PRAZO: 'MUDAR_PRAZO',
  MUDAR_CUSTO: 'MUDAR_CUSTO',
  MUDAR_CRONOGRAMA: 'MUDAR_CRONOGRAMA',
  MUDAR_EFETIVO: 'MUDAR_EFETIVO',
  MUDAR_PASSOS: 'MUDAR_PASSOS',
  MUDAR_CARREGANDO: 'MUDAR_CARREGANDO'
}

const mutations = {
  [types.MUDAR_CARREGANDO] (state, { carregando }) {
    state.carregando = carregando
  },
  [types.MUDAR_LOCAL] (state, { local }) {
    state.local = local
  },
  [types.MUDAR_AREA] (state, { area }) {
    state.area = area
  },
  [types.MUDAR_PAVIMENTOS] (state, { pavimentos }) {
    state.pavimentos = pavimentos
  },
  [types.MUDAR_ACABAMENTO] (state, { acabamento }) {
    state.acabamento = acabamento
  },
  [types.MUDAR_REVESTIMENTO] (state, { revestimento }) {
    state.revestimento = revestimento
  },
  [types.MUDAR_NOME] (state, { nome }) {
    state.nome = nome
  },
  [types.MUDAR_INICIO] (state, { inicio }) {
    state.inicio = inicio
  },
  [types.MUDAR_PRAZO] (state, { prazo }) {
    state.prazo = prazo
  },
  [types.MUDAR_CUSTO] (state, { custo }) {
    state.custo = custo
  },
  [types.MUDAR_CRONOGRAMA] (state, { cronograma }) {
    state.cronograma = cronograma
  },
  [types.MUDAR_EFETIVO] (state, { efetivo }) {
    state.efetivo = efetivo
  },
  [types.MUDAR_PASSOS] (state, { passos }) {
    state.passos = passos
  }
}

function buscarCusto (commit) {
  return axios.get('/custo/' + state.area + '/' + state.local + '/' + state.acabamento)
  .then(function (response) {
    commit(types.MUDAR_CUSTO, { custo: response.data })
  })
  .catch(function (error) {
    console.log(error)
  })
}

function buscarPrazo (commit) {
  return axios.get('/prazo/' + state.pavimentos + '/' + state.acabamento)
  .then(function (response) {
    commit(types.MUDAR_PRAZO, { prazo: response.data })
  })
  .catch(function (error) {
    console.log(error)
  })
}

const actions = {
  calcularResultado ({ commit }) {
    return new Promise(function (resolve, reject) {
      axios.all([buscarCusto(commit), buscarPrazo(commit)])
        .then(axios.spread(function (acct, perms) {
          axios.get('/efetivo/' + state.prazo + '/' + state.area + '/' + state.pavimentos)
          .then(function (response) {
            commit(types.MUDAR_EFETIVO, { efetivo: response.data })
            axios.get('/cronograma/' + state.prazo + '/' + state.custo.toFixed(0))
            .then(function (response) {
              commit(types.MUDAR_CRONOGRAMA, { cronograma: response.data.tarefas })
              resolve()
            })
            .catch(function (error) {
              console.log(error)
              reject(Error('It broke'))
            })
          })
          .catch(function (error) {
            console.log(error)
            reject(Error('It broke'))
          })
        }))
    })
  },
  modificarCronogramaPorPrazo ({ commit }, { novoPrazo }) {
    return new Promise(function (resolve, reject) {
      commit(types.MUDAR_PRAZO, { prazo: novoPrazo })
      axios.get('/cronograma/' + state.prazo + '/' + state.custo.toFixed(0))
      .then(function (response) {
        commit(types.MUDAR_CRONOGRAMA, { cronograma: response.data.tarefas })
        resolve()
      })
      .catch(function (error) {
        console.log(error)
        reject(Error('It broke'))
      })
    })
  },
  modificarCronogramaPorDados ({ commit }, { dadosEnvio }) {
    return new Promise(function (resolve, reject) {
      axios.get('/cronograma/', {
        dadosEnvio
      })
      .then(function (response) {
        commit(types.MUDAR_CRONOGRAMA, { cronograma: response.data.tarefas })
        resolve()
      })
      .catch(function (error) {
        console.log(error)
        reject(Error('It broke'))
      })
    })
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
