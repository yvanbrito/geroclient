import * as types from './mutation-types'
import axios from '../plugins/http'

export default {
  login ({ commit }, payload) {
    return new Promise(function (resolve, reject) {
      axios.post('/authenticate', {
        username: payload.email,
        password: payload.senha,
        rememberMe: payload.lembrar
      })
      .then(function (response) {
        commit(types.MUDAR_USUARIO, {
          id: response.data.usuarioId,
          nome: response.data.nome,
          email: response.data.login
        })
        commit(types.ADICIONAR_TOKEN, { token: response.data.id_token })
        resolve()
      })
      .catch(function (error) {
        console.log(error)
        reject('error')
      })
    })
  },
  logout ({ commit }) {
    return new Promise(function (resolve, reject) {
      commit(types.REMOVER_TOKEN)
      commit(types.ZERAR_INFORMACOES)
      resolve()
    })
  },
  atualizarUsuario ({ commit }, payload) {
    return axios.post('/registro/' + payload.id, {
      nome: payload.nome,
      email: payload.email,
      senha: payload.senha,
      id: payload.id
    })
    .then(function (response) {
      commit(types.MUDAR_USUARIO, {
        id: payload.id,
        nome: payload.nome,
        email: payload.email
      })
    })
    .catch(function (error) {
      console.log(error)
    })
  },
  registrarUsuario ({ commit }, payload) {
    return axios.post('/usuarios', {
      nome: payload.nome,
      email: payload.email,
      senha: payload.senha,
      criado: new Date()
    })
    .then(function (response) {
      console.log(response)
    })
    .catch(function (error) {
      console.log(error)
    })
  }
}
