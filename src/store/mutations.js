import * as types from './mutation-types.js'

export default {
  [types.ADICIONAR_TOKEN] (state, { token }) {
    window.localStorage.setItem('token', token)
    state.token = token
  },
  [types.REMOVER_TOKEN] (state) {
    window.localStorage.removeItem('token')
    state.token = ''
  },
  [types.MUDAR_USUARIO] (state, usuario) {
    state.usuario = usuario
  },
  [types.ZERAR_INFORMACOES] (state) {
    state.usuario = {}
    state.local = ''
    state.area = 0
    state.pavimentos = 0
    state.acabamento = 0
    state.revestimento = 0
    state.prazo = 0
    state.custo = 0
    state.efetivo = 0
    state.cronograma = []
  }
}
