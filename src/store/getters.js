import { isEmpty } from 'lodash'

export default {
  token: state => state.token,
  usuario: state => state.usuario,
  isLogged: state => !isEmpty(state.token)
}
