import Vue from 'vue'
import Vuex from 'vuex'
import state from './state'
import getters from './getters'
import mutations from './mutations'
import actions from './actions'
import chatbot from '../components/privado/chatbot/vuex'
import chatdinamico from '../components/privado/chatbotdinamico/vuex'
import perfilobras from '../components/privado/perfilobra/vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state,
  getters,
  actions,
  mutations,
  modules: {
    chatbot,
    perfilobras,
    chatdinamico
  }
})
