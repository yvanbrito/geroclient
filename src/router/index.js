import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/home/login'
import TelaTeste from '@/components/home'
import TelaLogin from '@/components/home/login/telaLogin'
import Registrar from '@/components/home/registrar'
import Gero from '@/components/privado'
import Usuario from '@/components/privado/usuario'
import Resultado from '@/components/privado/resultado'
import Chatbot from '@/components/privado/chatbot'
import store from '../store'
import ListaPerfilObras from '@/components/privado/perfilobra/listaobras'
import ChatDinamico from '@/components/privado/chatbotdinamico'
import Planejamento from '@/components/privado/planejamento'
import ProgSemanal from '@/components/privado/progsemanal'
import Dashboard from '@/components/privado/dashboard'
import Diagnostico from '@/components/privado/diagnostico'

Vue.use(Router)

const router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/teste',
      name: 'TelaTeste',
      component: TelaTeste
    },
    {
      path: '/login',
      name: 'TelaLogin',
      component: TelaLogin
    },
    {
      path: '/cadastre-se',
      name: 'Cadastro',
      component: Registrar
    },
    {
      path: '/gero',
      redirect: '/gero/listaperfilobras',
      name: 'Gero',
      component: Gero,
      children: [
        {
          path: 'chatbot',
          name: 'Chatbot',
          component: Chatbot,
          meta: { requiresAuth: false }
        },
        {
          path: 'usuario',
          name: 'Usuario',
          component: Usuario,
          meta: { requiresAuth: false }
        },
        {
          path: 'resultado',
          name: 'Resultado',
          component: Resultado,
          meta: { requiresAuth: false }
        },
        {
          path: 'listaperfilobras',
          name: 'ListaPerfilObras',
          component: ListaPerfilObras,
          meta: { requiresAuth: false }
        },
        {
          path: 'chatdinamico',
          name: 'ChatDinamico',
          component: ChatDinamico,
          meta: { requiresAuth: false }
        },
        {
          path: 'planejamento',
          name: 'Planejamento',
          component: Planejamento,
          meta: { requiresAuth: false }
        },
        {
          path: 'progsemanal',
          name: 'ProgSemanal',
          component: ProgSemanal,
          meta: { requiresAuth: false }
        },
        {
          path: 'dashboard',
          name: 'Dashboard',
          component: Dashboard,
          meta: { requiresAuth: false }
        },
        {
          path: 'diagnostico',
          name: 'Diagnostico',
          component: Diagnostico,
          meta: { requiresAuth: false }
        }
      ]
    },
    {
      path: '*',
      component: Home
    }
  ]
})

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    // this route requires auth, check if logged in
    // if not, redirect to login page.
    if (!store.getters.isLogged) {
      next({
        path: '/',
        query: { redirect: to.fullPath }
      })
    } else {
      next()
    }
  } else {
    next() // make sure to always call next()!
  }
})

export default router
