import axios from 'axios'

axios.defaults.baseURL = 'http://45.79.106.82/api'// 'http://192.168.0.105:8080/api'
axios.defaults.headers.common['Authorization'] = window.localStorage.getItem('token') === null ? '' : 'Bearer ' + window.localStorage.getItem('token')
axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded'

export default axios
