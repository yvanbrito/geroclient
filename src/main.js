import Vue from 'vue'
import App from './App'
import router from './router'
import 'font-awesome/css/font-awesome.min.css'
import store from './store'
import VueTyperPlugin from 'vue-typer'
import ZkTable from 'vue-table-with-tree-grid'

Vue.use(VueTyperPlugin)
Vue.use(ZkTable)

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App)
})
