import axios from '../plugins/http'
import store from '../store'

export const salvarObra = (payload) => {
  let acabamento = {1: 'NAO', 2: 'BAIXO', 3: 'MEDIO', 4: 'ALTO'}
  let revestimento = {0: 'NAO', 2: 'PINTURA', 3: 'CERAMICA'}
  return axios.post('/perfil-obras', {
    'area': payload.area,
    'local': payload.local,
    'pavimentos': payload.pavimentos,
    'tipoAcabamento': acabamento[payload.acabamento],
    'tipoRevestimento': revestimento[payload.revestimento],
    'usuarioId': payload.usuario.id,
    'usuarioNome': payload.usuario.nome,
    'nome': payload.nome,
    'inicio': payload.inicio
  })
  .then(function (response) {
    console.log(response)
  })
  .catch(function (error) {
    console.log(error)
  })
}

export const abrirCronograma = () => {
  return axios.post('/acessos', {
    data: new Date(),
    fonte: 'Usuário \'' + store.getters.usuario.email + '\' clicou no botão \'Ver Cronograma\'.',
    usuarioId: store.getters.usuario.id
  })
  .then(function (response) {
    console.log(response)
  })
  .catch(function (error) {
    console.log(error)
  })
}

export const clicarRecalcular = () => {
  return axios.post('/acessos', {
    data: new Date(),
    fonte: 'Usuário \'' + store.getters.usuario.email + '\' clicou no botão \'Refazer Cronograma\'.',
    usuarioId: store.getters.usuario.id
  })
  .then(function (response) {
    console.log(response)
  })
  .catch(function (error) {
    console.log(error)
  })
}

export const abrirHome = () => {
  return axios.post('/acessos', {
    data: new Date(),
    fonte: store.getters.usuario !== {} ? 'Usuário \'' + store.getters.usuario.email + '\' entrou na \'Home\'.'
    : 'Um visitante entrou na \'Home\'.',
    usuarioId: store.getters.usuario.id
  })
  .then(function (response) {
    console.log(response)
  })
  .catch(function (error) {
    console.log(error)
  })
}

export const editarPerfil = () => {
  return axios.post('/acessos', {
    data: new Date(),
    fonte: 'Usuário \'' + store.getters.usuario.email + '\' clicou no botão \'Voltar\'.',
    usuarioId: store.getters.usuario.id
  })
  .then(function (response) {
    console.log(response)
  })
  .catch(function (error) {
    console.log(error)
  })
}

export const voltarEdicao = () => {
  return axios.post('/acessos', {
    data: new Date(),
    fonte: 'Usuário \'' + store.getters.usuario.email + '\' clicou no botão \'Editar Perfil\'.',
    usuarioId: store.getters.usuario.id
  })
  .then(function (response) {
    console.log(response)
  })
  .catch(function (error) {
    console.log(error)
  })
}
